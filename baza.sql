drop table odabiri;
drop table ucenici;
drop table teme;
drop table razredi;
drop table predmeti;
drop table profesori;
drop table admini;

drop sequence admini_id_seq;
drop sequence ucenici_id_seq;
drop sequence teme_id_seq;
drop sequence razredi_id_seq;
drop sequence predmeti_id_seq;
drop sequence profesori_id_seq;
drop sequence odabiri_id_seq;

select * from ucenici;
select * from profesori;
select * from teme;
select * from admini;
select * from odabiri;
select * from predmeti;
select * from razredi;


CREATE TABLE profesori (
	id NUMBER(9, 0) NOT NULL,
	ime VARCHAR2(30) NOT NULL,
	prezime VARCHAR2(30) NOT NULL,
	created TIMESTAMP NOT NULL,
	updated TIMESTAMP,
	constraint PROFESORI_PK PRIMARY KEY (id));

CREATE sequence PROFESORI_ID_SEQ;

CREATE trigger BI_PROFESORI_ID
  before insert on profesori
  for each row
begin
   
  select PROFESORI_ID_SEQ.nextval into :NEW.id from dual;
  IF inserting then
    select systimestamp into :NEW.created from dual;
  end if;
  select systimestamp into :NEW.updated from dual;
  
end;

/
CREATE TABLE razredi (
	id NUMBER(9, 0) NOT NULL,
	naziv VARCHAR2(50) NOT NULL,
	created TIMESTAMP NOT NULL,
	updated TIMESTAMP,
	constraint RAZREDI_PK PRIMARY KEY (id));

CREATE sequence RAZREDI_ID_SEQ;

CREATE trigger BI_RAZREDI_ID
  before insert on razredi
  for each row
begin
   
  select RAZREDI_ID_SEQ.nextval into :NEW.id from dual;
  IF inserting then
    select systimestamp into :NEW.created from dual;
  end if;
  select systimestamp into :NEW.updated from dual;
  
end;

/
CREATE TABLE predmeti (
	id NUMBER(9, 0) NOT NULL,
	naziv VARCHAR2(50) NOT NULL,
	smjer VARCHAR2(30) NOT NULL,
	created TIMESTAMP NOT NULL,
	updated TIMESTAMP,
	razred NUMBER(1, 0),
	limit NUMBER(2, 0) NOT NULL,
	constraint PREDMETI_PK PRIMARY KEY (id));

CREATE sequence PREDMETI_ID_SEQ;

CREATE trigger BI_PREDMETI_ID
  before insert on predmeti
  for each row
begin
   
  select PREDMETI_ID_SEQ.nextval into :NEW.id from dual;
  IF inserting then
    select systimestamp into :NEW.created from dual;
  end if;
  select systimestamp into :NEW.updated from dual;
  
end;

/
CREATE TABLE ucenici (
	id NUMBER(9, 0) NOT NULL,
	idRazreda INT NOT NULL,
	ime VARCHAR2(50) NOT NULL,
	prezime VARCHAR2(50) NOT NULL,
	email VARCHAR2(30) UNIQUE NOT NULL,
	hash TIMESTAMP UNIQUE NOT NULL,
	smjer VARCHAR2(30) NOT NULL,
	created TIMESTAMP NOT NULL,
	updated TIMESTAMP,
	constraint UCENICI_PK PRIMARY KEY (id));

CREATE sequence UCENICI_ID_SEQ;

CREATE trigger BI_UCENICI_ID
  before insert on ucenici
  for each row
begin
   
  select UCENICI_ID_SEQ.nextval into :NEW.id from dual;
  IF inserting then
    select systimestamp into :NEW.created from dual;
  end if;
  select systimestamp into :NEW.updated from dual;
  
end;

/
CREATE TABLE teme (
	id NUMBER(9, 0) NOT NULL,
	idRazreda NUMBER(9, 0) NOT NULL,
	idPredmeta NUMBER(9, 0) NOT NULL,
	idProfesora NUMBER(9, 0) NOT NULL,
	naziv VARCHAR2(100) NOT NULL,
	created TIMESTAMP NOT NULL,
	updated TIMESTAMP,
	constraint TEME_PK PRIMARY KEY (id));

CREATE sequence TEME_ID_SEQ;

CREATE trigger BI_TEME_ID
  before insert on teme
  for each row
begin
   
  select TEME_ID_SEQ.nextval into :NEW.id from dual;
  IF inserting then
    select systimestamp into :NEW.created from dual;
  end if;
  select systimestamp into :NEW.updated from dual;
  
end;
/
CREATE TABLE odabiri (
	id NUMBER(9, 0) NOT NULL,
	idTeme NUMBER(9, 0) NOT NULL,
	idUcenika NUMBER(9, 0) NOT NULL,
	created TIMESTAMP NOT NULL,
	updated TIMESTAMP,
	constraint ODABIRI_PK PRIMARY KEY (id));

CREATE sequence ODABIRI_ID_SEQ;

CREATE trigger BI_ODABIRI_ID
  before insert on odabiri
  for each row
begin
   
  select ODABIRI_ID_SEQ.nextval into :NEW.id from dual;
  IF inserting then
    select systimestamp into :NEW.created from dual;
  end if;
  select systimestamp into :NEW.updated from dual;
  
end;

/
CREATE TABLE admini (
	id NUMBER(9, 0) NOT NULL,
	ime VARCHAR2(50) NOT NULL,
	prezime VARCHAR2(50) NOT NULL,
	email VARCHAR2(30) UNIQUE NOT NULL,
	password VARCHAR2(30) NOT NULL,
	created TIMESTAMP NOT NULL,
	updated TIMESTAMP,
	ovlasti NUMBER(1, 0) NOT NULL,
	constraint ADMINI_PK PRIMARY KEY (id));

CREATE sequence ADMINI_ID_SEQ;

CREATE trigger BI_ADMINI_ID
  before insert on admini
  for each row
begin
   
  select ADMINI_ID_SEQ.nextval into :NEW.id from dual;
  IF inserting then
    select systimestamp into :NEW.created from dual;
  end if;
  select systimestamp into :NEW.updated from dual;
  
end;

/

ALTER TABLE ucenici ADD CONSTRAINT ucenici_fk0 FOREIGN KEY (idRazreda) REFERENCES razredi(id);

ALTER TABLE teme ADD CONSTRAINT teme_fk0 FOREIGN KEY (idRazreda) REFERENCES razredi(id);
ALTER TABLE teme ADD CONSTRAINT teme_fk1 FOREIGN KEY (idPredmeta) REFERENCES predmeti(id);
ALTER TABLE teme ADD CONSTRAINT teme_fk2 FOREIGN KEY (idProfesora) REFERENCES profesori(id);

ALTER TABLE odabiri ADD CONSTRAINT odabiri_fk0 FOREIGN KEY (idTeme) REFERENCES teme(id);
ALTER TABLE odabiri ADD CONSTRAINT odabiri_fk1 FOREIGN KEY (idUcenika) REFERENCES ucenici(id);


